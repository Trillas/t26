package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T26Actividad4Application {

	public static void main(String[] args) {
		SpringApplication.run(T26Actividad4Application.class, args);
	}

}

package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.MaquinasRegistradas;
import com.example.demo.dto.Venta;
import com.example.demo.service.VentaServiceImpl;

@RestController
@RequestMapping("/api")
public class VentaController {

	@Autowired
	VentaServiceImpl ventaServiceImpl;
	
	@GetMapping("/venta")
	public List<Venta> listarVentas() {
		return ventaServiceImpl.listarVenta();
		
	}
	
	@PostMapping("/venta")
	public Venta guardarVenta(@RequestBody Venta venta) {
		return ventaServiceImpl.guardarVenta(venta);
		
	}
	
	@GetMapping("/venta/{id}")
	public Venta ventaID(@PathVariable(name = "id") int id) {
		
		Venta venta_id = new Venta();
		
		venta_id = ventaServiceImpl.ventaID(id);
		
		System.out.println("venta ID: " + venta_id);
		
		return venta_id;
		
	}
	
	@PutMapping("/venta/{id}")
	public Venta actualizarVenta(@PathVariable(name = "id") int id, @RequestBody Venta venta) {
		
		Venta venta_sel = new Venta();
		Venta venta_act = new Venta();
		
		venta_sel = ventaServiceImpl.ventaID(id);
		
		venta_sel.setCajeros(venta.getCajeros());
		venta_sel.setMaquinas(venta.getMaquinas());
		venta_sel.setProductos(venta.getProductos());
		
		venta_act = ventaServiceImpl.actualizarVenta(venta_sel);
		
		return venta_act;
		
	}
	
	@DeleteMapping("/venta/{id}")
	public void eliminarVenta(@PathVariable(name = "id") int id) {
		ventaServiceImpl.eliminarVenta(id);
	}
}

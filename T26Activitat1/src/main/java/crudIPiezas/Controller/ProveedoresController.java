package crudIPiezas.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import crudIPiezas.Dto.Proveedores;
import crudIPiezas.Service.ProveedoresServiceImpl;

@RestController
@RequestMapping("/api")
public class ProveedoresController {

	@Autowired
	ProveedoresServiceImpl ProveedoresServiceImpl;

	@GetMapping("/Proveedores")
	public List<Proveedores> listarProveedores() {
		return ProveedoresServiceImpl.listarProveedores();
	}

	@PostMapping("/Proveedores")
	public Proveedores guardarProveedores(@RequestBody Proveedores Proveedores) {
		return ProveedoresServiceImpl.guardarProveedor(Proveedores);
	}

	@GetMapping("/Proveedores/{id}")
	public Proveedores ProveedoresXID(@PathVariable(name = "id") int id) {

		Proveedores Proveedores_xid = new Proveedores();

		Proveedores_xid = ProveedoresServiceImpl.proveedorXID(id);

		System.out.println("Proveedores XID: " + Proveedores_xid);

		return Proveedores_xid;
	}

	@PutMapping("/Proveedores/{id}")
	public Proveedores actualizarProveedores(@PathVariable(name = "id") int id, @RequestBody Proveedores Proveedores) {

		Proveedores Proveedores_seleccionado = new Proveedores();
		Proveedores Proveedores_actualizado = new Proveedores();

		Proveedores_seleccionado = ProveedoresServiceImpl.proveedorXID(id);

		Proveedores_seleccionado.setNombre(Proveedores.getNombre());

		Proveedores_actualizado = ProveedoresServiceImpl.actualizarProveedor(Proveedores_seleccionado);

		System.out.println("El Proveedores actualizado es: " + Proveedores_actualizado);

		return Proveedores_actualizado;
	}

	@DeleteMapping("/Proveedores/{id}")
	public void eliminarProveedores(@PathVariable(name = "id") int id) {
		ProveedoresServiceImpl.eliminarProveedor(id);
	}

}

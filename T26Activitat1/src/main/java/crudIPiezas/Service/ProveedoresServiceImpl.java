package crudIPiezas.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crudIPiezas.Dao.ProveedoresDao;
import crudIPiezas.Dto.Proveedores;

@Service
public class ProveedoresServiceImpl implements IProveedoresService {
	@Autowired
	ProveedoresDao proveedoresDao;

	@Override
	public List<Proveedores> listarProveedores() {
		return proveedoresDao.findAll();
	}

	@Override
	public Proveedores guardarProveedor(Proveedores proveedores) {
		// TODO Auto-generated method stub
		return proveedoresDao.save(proveedores);
	}

	@Override
	public Proveedores proveedorXID(int id) {

		return proveedoresDao.findById(id).get();
	}

	@Override
	public Proveedores actualizarProveedor(Proveedores proveedores) {

		return proveedoresDao.save(proveedores);
	}

	@Override
	public void eliminarProveedor(int id) {
		proveedoresDao.deleteById(id);

	}

}

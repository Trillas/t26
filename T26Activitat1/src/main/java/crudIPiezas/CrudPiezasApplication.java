package crudIPiezas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudPiezasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudPiezasApplication.class, args);
	}

}

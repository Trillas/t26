package crudIPiezas.Dao;

import org.springframework.data.jpa.repository.JpaRepository;

import crudIPiezas.Dto.Piezas;

public interface PiezasDao extends JpaRepository<Piezas, Integer> {

}
